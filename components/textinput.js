import React from 'react';
import { TouchableOpacity, Text, TextInput, StyleSheet, View } from 'react-native';
import { Buttons, Formatting, Typography, Colors } from '../styles';

export default function (props) {
  return (
    <View style={[styles.textinput]}>
        <TextInput {...props} style={ [Typography.base, props.style] }/>
    </View>
  );
}



const styles = StyleSheet.create({
  textinput: {
    ...Formatting.padding,
    ...Formatting.rounded,
    ...Formatting.paddingSidesExtra,
    ...Formatting.shadow(5),
    ...Formatting.marginSides,
    ...Colors.bgWhite
  }
})
