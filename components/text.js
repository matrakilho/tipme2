import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Typography } from '../styles';

export default function (props) {
  return (
    <Text {...props} style={ [Typography.base, props.style] }/>
  );
}
