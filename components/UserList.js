import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
  StatusBar
} from 'react-native';

import {
  ListItem,
  SearchBar
} from 'react-native-elements';

import { Button, Text, Icon, TextInput } from '.';

import { Colors, Formatting, Typography } from '../styles';

import { MonoText } from '../components/StyledText';

export default class UserList extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
        users: Object.keys(props.users),
        searchFor: ""
    };
  }

  onEditSearch (e) {
    let elc = e.toLowerCase();
    this.setState({
      users: Object.keys(this.props.users).filter( (s) => {
        return s.toLowerCase().includes(elc)
      }),
      searchFor: e
    });
  }


  onPressItem (item) {
      this.props.onPressItem(item)
  }

  userItem ( {item} )  {
    return <ListItem
        title= {item}
        leftAvatar={{
          source : { uri: '../assets/images/user.png' },
          title: item[0]
        }}
        chevron
        bottomDivider
        titleStyle={Typography.base}
        onPress = { (e) => this.onPressItem.bind(this)(item)}
        //subtitle={item.email}
      />
  }

  render () {
    let props = this.props;
    let {searchFor, users} = this.state;

    return (
      <View style={styles.container}>
        <FlatList
          data = {users}
          renderItem = {this.userItem.bind(this)}
          ListHeaderComponent={
            <SearchBar style={styles.searchbox}
            inputStyle={Typography.base}
            value={searchFor}
            placeholder="Search for user"
            onChangeText = { this.onEditSearch.bind(this) }
            onCancel = {() => this.setState({searchFor: ""})}
            lightTheme
            round
              />
          }
          keyExtractor={ (item) => item }
          />
      </View>
    )};
}

/*HomeScreen.navigationOptions = (props) => {
  console.log("Home screen props");
  console.log(props);
  return { title: "Welcome, $" + props.username};
};*/


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // Only works on Android
    //marginTop: StatusBar.currentHeight
  },
  userName: {
  },
  userItem: {
    flex : 1,
    width: "100%",
    height: 40,
    ...Formatting.paddingExtra,
    justifyContent: 'center',
  },
  separator: {
    height: 1,
    width: '86%',
    backgroundColor: '#CED0CE',
    marginLeft: '14%',
  },
  searchbox: {

  }
});
