import React from 'react';
import { Text } from 'react-native';
import { Typography } from '../styles';

export default function (props) {
  return (
    <Text style={Typography.h1}>{ "Welcome, $" + props.username }</Text>
  );
}
