import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import  Text  from './text';
import  Icon from './icon';
import { Formatting } from '../styles';
import Colors from '../constants/Colors';

export default function (props) {
  let {style,...otherProps} = props;

  return (
    <TouchableOpacity
        {...otherProps}
         style={[styles.button, style]}
    >
    { props.icon &&
      <Icon
        {...props.icon }
        style={styles.text}
      />
    }
    <Text style={styles.text}>{ props.title }</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    ...Formatting.rounded,
    ...Formatting.shadow(5),
    ...Formatting.padding,
    // Flex messes with padding
    //flex: 1,
    //flexDirection: 'row'
  },
  text: {
    width: "100%",
    ...Formatting.center,
  }
})
