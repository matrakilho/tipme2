import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Formatting } from '../styles';

export default function (props) {
  return (
    <View {...props} style={[props.style, styles.panel]}>
      {props.children}
    </View>
  );
}

const styles = StyleSheet.create({
  panel: {
    ...Formatting.paddingExtra,
    ...Formatting.rounded,
    ...Formatting.shadow(5),
    ...Formatting.margin
  }
})
