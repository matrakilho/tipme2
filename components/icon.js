import React from 'react';
import { Ionicons } from '@expo/vector-icons';

import Colors from '../constants/Colors';

export default function (props) {
  return (
    <Ionicons
      {...props}
      size={20}
    />
  );
}
