import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native';

import Colors from '../constants/Colors';

export default function IconButton(props) {
  return (
    <TouchableOpacity
      onPress={props.onPress}>
      <Ionicons
        name={props.name}
        size={26}
        style={{ margin: 5 }}
        color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    </TouchableOpacity>
  );
}
