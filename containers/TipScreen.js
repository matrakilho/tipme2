import { connect } from 'react-redux'
import { tip } from '../actions'
import TipScreen from '../screens/TipScreen'

const mapStateToProps = state => {
  return {
    username: state.username,
    diamonds: state.wallet.diamonds
  };
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  onTip: (from, to, type, amount) => dispatch(tip(from, to, type, amount))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TipScreen)
