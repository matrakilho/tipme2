import { connect } from 'react-redux'
import HomeScreen from '../screens/HomeScreen'

const mapStateToProps = state => {
  let { username, wallet, history, tips } = state;
  console.log('Container of HomeScreen');
  console.log(state);
  return { username, wallet, history, tips };
}

const mapDispatchToProps = (dispatch, ownProps) => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)
