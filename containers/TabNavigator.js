import React from 'react';
import { connect } from 'react-redux'
import { login } from '../actions'
import TabNavigator from '../navigation/MainTabNavigator'

//const mapStateToProps = state => ({
//  username: state.username
//})

const mapStateToProps = state => {
  console.log("state from TabNavigator container");
  console.log(state);
  return { username: state.username };
}

const mapDispatchToProps = dispatch => ({

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabNavigator)
