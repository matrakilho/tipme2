import { connect } from 'react-redux'
import { login } from '../actions'
import LoginScreen from '../screens/LoginScreen'

const mapStateToProps = state => ({
  username: state.username
})

const mapDispatchToProps = dispatch => ({
  onLogin: username => dispatch(login(username))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen)
