import connect from './thunk'
//import { connect } from 'react-redux'
import { getUsers } from '../actions'
import UserList from '../components/UserList'

//const mapStateToProps = state => ({
//  username: state.username
//})

const mapStateToProps = state => {
  return { username: state.username };
}

const mapDispatchToProps = dispatch => ({

})

/*export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList)*/


export default connect( () => {
  return getUsers().then ( users => {
    return { users };
  });
})(UserList)
