import React, {Component} from 'react';
import {
  View,
} from 'react-native';
/*
 How it should be called?

 import link from './thunk.js';

 const Models = link(function() {
   return fetch('/api/models/')
   .then( res => res.json())
 })(Models)

This is a higher-order component (HOC)
*/

export default (promise) => (C, loader = null) => {
    return class extends Component {

        fetchData(nextProps, prevProps = null) {
          //this.state = null;
          //this.forceUpdate();
          //this.setState({ __loading : true })
          const p = promise(nextProps, prevProps);
          p && p.then( res => {
            this.setState(res);
            // try this if setState does not work
            //this.state = res;
            //this.forceUpdate();
          })
        }

        componentDidMount() {
          this.refresh()
          // returns a callback to force fetch of new data
          this.props.refresh && this.props.refresh( this.fetchData.bind(this) )
        }

        componentWillReceiveProps(nextProps) {
          // got new props -> fetch new data
          this.fetchData(nextProps, this.props);
        }

        refresh () {
          this.fetchData(this.props)
        }

        render() {
            return this.state ? <C refresh={ this.refresh.bind(this) } {...this.props} {...this.state}/> : loader;
        }
    }
};
