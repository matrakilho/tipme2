import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
  StatusBar
} from 'react-native';

import { Button, Text, Icon, IconButton } from '../components';
import Items from '../constants/items';

import { Colors, Formatting, Typography } from '../styles';

import { MonoText } from '../components/StyledText';



function historyItem (item)  {

  function text (item) {
    item = item.item;

    switch (item.type) {
      case 'buy':
        return <View style={styles.historyItem}>
          <Image style={styles.thumbnailImage} source={require('../assets/images/diamond.png')}/>
          <Text>Bought {item.amount} diamonds</Text>
        </View>
      case 'sell':
        return <View style={styles.historyItem}>
          <Image style={styles.thumbnailImage} source={require('../assets/images/coin.png')}/>
          <Text>Cashed out {item.amount} coins</Text>
        </View>
      case 'send':
        return <View style={styles.historyItem}>
          <Image style={styles.thumbnailImage} source={require('../assets/images/logo.png')}/>
          <Text>Tipped {item.to} with {item.amount} </Text>
          <Image style={styles.thumbnailImage} source={Items[item.itemType].icon}/>
        </View>
      case 'receive':
        return <View style={styles.historyItem}>
          <Image style={styles.thumbnailImage} source={require('../assets/images/logo.png')}/>
          <Text>Received {item.amount} </Text>
          <Image style={styles.thumbnailImage} source={Items[item.itemType].icon}/>
          <Text> from {item.from} </Text>
        </View>
    }
  }

  return (text(item));
}

export default class HomeScreen extends React.Component {

  componentDidMount() {
      this.props.navigation.setParams({ username: this.props.username });
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: `Welcome, ${navigation.getParam('username')}`,
    headerRight: (
      <IconButton
        name={Platform.OS === 'ios' ? 'ios-log-out' : 'md-log-out'}
        onPress={() => navigation.navigate('Login')}
      />
    ),
    //header: null
  })

  render () {
    let {navigation, wallet, tips, username } = this.props;
    let tipsArray = Object.keys(tips).map( key => {
      let tip = tips[key];
      return {
        key,
        type: tip.from == username ? 'send' : 'receive',
        amount: tip.amount,
        to: tip.to,
        from: tip.from,
        itemType: tip.type,
        timestamp: new Date(tip.timestamp.seconds*1000)
      }
    });

    tipsArray.sort( (b,a) => a.timestamp - b.timestamp);

    console.log(tipsArray);
    const history = [
      {
        key: 3243245324,
        type: 'buy',
        amount: 100
      },
      {
        key: 457657432456,
        type: 'sell',
        amount: 24.32
      },
      ...tipsArray
    ];



  return (
    <View style={styles.container}>
        <Text style={{...Typography.section, ...Formatting.paddingSidesExtra}}>
          Wallet:
        </Text>

        <View style={styles.row}>
          <View>
            <Image
              source={require('../assets/images/diamond.png')}
              style={styles.iconImage}
            />
          </View>
          <View>
            <Text style={styles.amount}>{wallet.diamonds}</Text>
          </View>
          <View>
            <Button title=" Buy "
            style={styles.button}/>
          </View>
        </View>

        <View style={styles.row}>
        <View>
          <Image
            source={require('../assets/images/coin.png')}
            style={styles.iconImage}
          />
        </View>
        <View>
          <Text style={styles.amount}>{wallet.coins}</Text>
        </View>
        <View>
          <Button title="Cashout"
           style={styles.button} />
        </View>
        </View>

        <View style={styles.getStartedContainer}>
          <Button title="Send a tip"
            icon={{
              name: Platform.OS === 'ios' ? 'ios-heart' : 'md-heart',
              size: 15,
              color: "red"
            }}
            onPress={ (e) => navigation.navigate('Users') }
            style={styles.tipButton}
          />
        </View>

        <View style={{flex:1}}>
          <Text style={{...Typography.section, ...Formatting.paddingExtra}}>
            Recent history:
          </Text>

          <ScrollView>
              <FlatList
                data={history}
                renderItem={ historyItem }
              />
          </ScrollView>

        </View>

      {/*<View style={styles.tabBarInfoContainer}>
        <Text style={styles.tabBarInfoText}>
          This is a tab bar. You can edit it in:
        </Text>

        <View
          style={[styles.codeHighlightContainer, styles.navigationFilename]}>
          <MonoText style={styles.codeHighlightText}>
            navigation/MainTabNavigator.js
          </MonoText>
        </View>
      </View>
      */}
    </View>
  )};
}


function DevelopmentModeNotice() {
  if (__DEV__) {
    const learnMoreButton = (
      <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
        Learn more
      </Text>
    );

    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use
        useful development tools. {learnMoreButton}
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/workflow/development-mode/'
  );
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/workflow/up-and-running/#cant-see-your-changes'
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // Only works on Android
    marginTop: StatusBar.currentHeight
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    ...Formatting.margin,
    alignItems : 'center'
  },
  iconImage: {
    width: 50,
    height: 50
  },
  amount: {
    fontSize: 32
  },
  thumbnailImage: {
    width: 16,
    height: 16,
    marginRight: 10
  },
  button: {
    ...Colors.bgMain,
    width: 100
  },
  tipButton: {
    backgroundColor: '#EEEEEE',
    marginVertical: 20,
    width: 100
  },
  historyItem: {
    width: "100%",
    height: 30,
    ...Formatting.paddingExtra,
    flex : 1,
    flexDirection: 'row',
    alignItems : 'center'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
