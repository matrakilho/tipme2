import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
  StatusBar
} from 'react-native';

import { Button, Text, Icon, TextInput } from '../components';
import { UserList } from '../containers';

import { Colors, Formatting, Typography } from '../styles';

export default class UsersScreen extends React.Component {

  static navigationOptions = {
    title: 'Select user'
  }

  render () {
    return (
      <UserList
      onPressItem = {(item) => this.props.navigation.navigate('Tip',
      {
        user: item
      }) }/>
    )};
}

/*HomeScreen.navigationOptions = (props) => {
  console.log("Home screen props");
  console.log(props);
  return { title: "Welcome, $" + props.username};
};*/


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // Only works on Android
    marginTop: StatusBar.currentHeight
  }
});
