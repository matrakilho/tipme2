import React from 'react';
import { View } from 'react-native';
import { Text, TextInput } from '../components';

export default function SettingsScreen() {
  /**
   * Go ahead and delete ExpoConfigView and replace it with your content;
   * we just wanted to give you a quick view of your config.
   */
  return <View>
  <Text>Username</Text>
  <TextInput value="" placeholder="Insert your username"/>
  </View>;
}

SettingsScreen.navigationOptions = {
  header: null
};
