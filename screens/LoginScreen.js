import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import { Typography, Formatting, Colors, Buttons } from '../styles';
import { login } from '../actions'

import {
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  ImageBackground,
  StatusBar
} from 'react-native';

import {
  Text,
  Button,
  TextInput,
  Panel
} from '../components';

import { MonoText } from '../components/StyledText';


export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {username: props.username};

    StatusBar.setTranslucent(false);
  }

  onEdit(e) {
      this.setState({username: e});
  }

  onSubmit(e) {
    this.props.onLogin(this.state.username).then ( () => {
      this.props.navigation.navigate('Home');
    });
  }

  render() {
  return (
    <ImageBackground source={require("../assets/images/login-background2.jpg")}
      style={styles.imageBackground}>

      <View style={styles.container}>

        <Image source={require("../assets/images/logo.png")}
        style={styles.logo}/>

        <View style={styles.usernameContainer}>
          <TextInput value={this.state.username}
            onSubmitEditing = { this.onSubmit.bind(this) }
            onChangeText = { this.onEdit.bind(this) }
            style = { styles.username }
            placeholder = "Username"
            autoFocus = {true} />
        </View>

        <View style={styles.loginContainer}>
            <Button title="Login" style={styles.loginButton}
            onPress={this.onSubmit.bind(this)}/>
        </View>

      </View>
    </ImageBackground>
  );
}
}

LoginScreen.navigationOptions = {
  header: null
};


const styles = StyleSheet.create({
  imageBackground: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',

  },
  container: {
    ...Formatting.center
  },
  usernameContainer: {
    margin: 60,
    width: "100%"
  },
  username: {
    ...Formatting.center
  },
  loginContainer: {
    paddingHorizontal:60,
    width: "100%"
  },
  loginButton: {
    ...Buttons.main,
  },
  logo: {
    width: 200,
    height: 200,
    marginTop: 100,
  }
});
