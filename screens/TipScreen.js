import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
  StatusBar
} from 'react-native';

import { Button, Text, Icon, TextInput } from '../components';

import { Colors, Formatting, Typography } from '../styles';

import Items from '../constants/items';

var itemArray = Object.keys(Items).map( (key) => ({
  type: key,
  ...Items[key]
}));


export default class TipScreen extends React.Component {

  static navigationOptions = ({navigation}) => ({
    title: 'Tip $' + navigation.getParam('user')
  })

  constructor (props) {
      super(props);
      this.handleSelectItem   = this.handleSelectItem.bind(this);
      this.handleChangeAmount = this.handleChangeAmount.bind(this);
      this.handleTip          = this.handleTip.bind(this);
  }

  state = {
    selectedItem : null,
    amount : 1
  }

  async handleTip () {
    let {selectedItem, amount} = this.state;
    let {username, navigation} = this.props;
    await this.props.onTip(
      username,
      navigation.getParam('user'),
      selectedItem,
      amount
    );
    navigation.navigate('Home');
  }

  handleSelectItem (itemType) {
    this.setState({
      selectedItem: itemType
    })
  }

  handleChangeAmount (val) {
    if (parseInt(val) || val == "")
      this.setState({
        amount: parseInt(val) || ""
      })
  }

  render () {
    let {selectedItem, amount} = this.state;
    let invalidAmount = (amount == "");
    if (selectedItem)
      var insufficientFunds = amount*Items[selectedItem].diamonds > this.props.diamonds;

    return (
      <View style={Formatting.center}>
        <FlatList
          data={itemArray}
          keyExtractor={item => item.type}
          numColumns={3}
          selected = {selectedItem}
          renderItem={ ({item}) => {
            return <View
              style={
                (item.type == selectedItem) &&
                styles.selectedItem
              } >
            <View style={styles.item}>
            <TouchableOpacity
              onPress={ () => this.handleSelectItem(item.type) }>
              <Image
                source={item.icon}
              />
            </TouchableOpacity>
            </View>
            </View>
          }}
        />
        {
          selectedItem &&
          <View style={[
            Formatting.paddingExtra,
            Formatting.center
            ]}>
            <TextInput style={styles.amount}
              value={amount.toString()}
              onChangeText={ this.handleChangeAmount }
            />
            <Text style={[Formatting.paddingExtra, Colors.gray]}>
            {"This tip will cost you " + amount*Items[selectedItem].diamonds }
            <Image
              source={require('../assets/images/diamond.png')}
              style={styles.iconImage}
            />
            </Text>
            { insufficientFunds &&
            <Text>You don't have enough diamonds</Text>}

            <Button title="Tip"
              style={styles.button}
              disabled={invalidAmount || insufficientFunds}
              onPress={this.handleTip.bind(this)}>
            </Button>
          </View>
        }
      </View>
    )};
}


const styles = StyleSheet.create({
  iconImage: {
    height: 16,
    width: 16
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  button: {
    ...Colors.bgMain,
    alignSelf: "stretch"
  },
  item: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    padding: 20,
  },
  selectedItem: {
    backgroundColor: '#EEEEEE',
    ...Formatting.rounded
  },
  amount: {
    ...Formatting.fullWidth,
    ...Formatting.center,
  }
});
