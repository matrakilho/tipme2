const initialState = {
  username: 'Hugo'
};

import {
  LOGIN,
  TIP,
  TIPPED
} from '../actions';

import { diamonds, coins } from '../constants/items';


export default function (state = initialState, action) {
  switch (action.type) {
      case LOGIN:
        console.log("User '" + action.username + "' logged in.");
        let {username, history, wallet, tips} = action;
        return {
          ...state,
          username,
          history,
          wallet,
          tips
        }
      case TIP:
        state.tips[action.db.id] = action.db.data;
        return {
          ...state,
          wallet: {
            diamonds: state.wallet.diamonds - action.amount*diamonds(action.itemType),
            coins: state.wallet.coins
          },
          tips: state.tips
        }
      case TIPPED:
        state.tips[action.db.id] = action.db.data;
        return {
          ...state,
          wallet: {
            diamonds: state.wallet.diamonds,
            coins: state.wallet.coins + action.amount*coins(action.itemType)
          },
          tips: state.tips
        }
        return
      default:
        return state;
  }
}
