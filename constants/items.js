export default items = {
  "flower": { diamonds: 1 , coins:  1, icon: require("../assets/images/rose.png") },
  "heart" : { diamonds: 10, coins: 10, icon: require("../assets/images/heart.png") },
  "teddy" : { diamonds: 50, coins: 50, icon: require("../assets/images/teddy-bear.png") },

  "car"   : { diamonds: 1 , coins:  1, icon: require("../assets/images/car.png") },
  "ball"  : { diamonds: 5 , coins:  5, icon: require("../assets/images/soccer-ball.png") },
  "dragon": { diamonds: 10, coins: 10, icon: require("../assets/images/dragon.png") },

  "like"  : { diamonds:  1, coins:  1, icon: require("../assets/images/like.png") },
  "badge" : { diamonds:  5, coins:  5, icon: require("../assets/images/medal.png") },
  "gift"  : { diamonds: 10, coins: 10, icon: require("../assets/images/gift.png") },
}

export const diamonds =  (type) => {
  return items[type].diamonds;
}

export const coins = (type) => {
  return items[type].coins;
}
