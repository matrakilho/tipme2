import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import UsersScreen from '../screens/UsersScreen';
//import TipScreen from '../screens/TipScreen';

import {
  LoginScreen,
  TipScreen,
  HomeScreen
} from '../containers';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

export const LoginStack = createStackNavigator(
  {
    Login: LoginScreen
  },
  config
);

LoginStack.navigationOptions = {
  header: null,
};

LoginStack.path = '';

export const HomeStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

HomeStack.path = '';

export const HistoryStack = createStackNavigator(
  {
    Links: LinksScreen,
  },
  config
);

HistoryStack.navigationOptions = {
  tabBarLabel: 'History',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'} />
  ),
};

HistoryStack.path = '';


// Navigator for bottom menu
const TabNavigator = createBottomTabNavigator({
  HomeStack,
  HistoryStack,
});

TabNavigator.path = '';
TabNavigator.navigationOptions = {
  header: null
}

// Navigator for Tipping
export default createStackNavigator({
  Login: LoginStack,
  Menu: TabNavigator,
  Users: UsersScreen,
  Tip: TipScreen
});
