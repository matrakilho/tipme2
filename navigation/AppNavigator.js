import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import {HomeStack, LoginStack} from './MainTabNavigator';
import MainNavigator from '../containers/TabNavigator';

export default createAppContainer(
  MainNavigator
  /*createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: LoginStack,
    Home: MainNavigator
  },{
    initialRouteName: 'Main'
  })*/
);
