import * as Colors from './colors';

export const base = {
  alignItems: 'center',
  padding: 10
}

export const rounded = {
  ...base,
  borderRadius: 30
};

export const small = {
  paddingHorizontal: 10,
  paddingVertical: 12,
  width: 75
};

export const main = {
  ...Colors.bgMain
}

export const smallRounded = {
  ...base,
  ...small,
  ...rounded
};
