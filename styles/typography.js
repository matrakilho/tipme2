export const base = {
  fontFamily: 'Averta',
  fontSize: 16
};

export const section = {
  ...base,
  fontSize: 11,
  color: 'gray',
  textTransform: 'uppercase',
  fontWeight: 'bold'
}

export const h1 = {
  ...base,
  fontSize: 24
}
