import * as Buttons from './buttons';
//import * as Colors from './colors'
//import * as Spacing from './spacing'
import * as Typography from './typography'
import * as Formatting from './formatting'
import * as Colors from './colors'

export { Buttons, Formatting, Typography, Colors }
