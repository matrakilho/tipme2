export const fullWidth = {
  width: "100%"
};

export const padding = {
  padding: 10
};

export const paddingExtra = {
  padding: 20
};

export const paddingSidesExtra = {
  paddingHorizontal: 20
};

export const margin = {
  margin: 20
};

export const marginSides = {
  marginHorizontal: 20,
};

export const rounded = {
  borderRadius: 20
};

export const center = {
  textAlign: 'center',
  alignItems: 'center'
}

export function shadow(elevation) {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}
