var firebase = require("firebase/app");
require('firebase/firestore');

/*var admin = require("firebase-admin");
var serviceAccount = require("../credentials/firecloud.json");
*/
// Your web app's Firebase configuration
var firebaseConfig = require("../credentials/firebase.json");

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();

export function collection (collection) {
  // Returns a CollectionReference
  return db.collection(collection);
}

export function doc (collection ,doc) {
  return db.collection(collection).doc(doc);
}

export async function get (collection, doc = null) {
  if (typeof collection != 'string')  // assume it is a reference to something
    var snap = await collection.get();
  else if (doc)
    var snap = await db.collection(collection).doc(doc).get();
  else
    var snap = await db.collection(collection).get();

  if (snap.exists) {
    // QueryDocumentSnapshot
    return snap.data();
  } else {
    // QuerySnapshot
    let ret = {}
    snap.docs.forEach ( (docsnap) => {
      ret[docsnap.id] = docsnap.data();
    })
    return ret;
  }
}

export async function add(collection, data) {
  return await db.collection(collection).add(data);
}

export function set(collection, doc, data) {

  // Add a new document in collection "users" with ID username
  let setDoc = db.collection(collection).doc(doc).set(data);

  /*admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://tipme-42f33.firebaseio.com"
  });

  // As an admin, the app has access to read and write all data, regardless of Security Rules
  var db = admin.database();*/
}

export function update(collection, doc, data) {
  db.collection(collection).doc(doc).update(data);
}

export function del(collection, doc, field = null) {
  let docRef = db.collection(collection).doc(doc);
  if (field) {
    docRef.update({
      [field] : db.FieldValue.delete()
    });
  } else {
    docRef.delete();
  }
}

export function append (collection, doc, field, item, t = null) {
  t = t || db;
  t.collection(collection).doc(doc).update({
    [field]: firebase.firestore.FieldValue.arrayUnion(item)
  });
}

export function increment (collection, doc, field, value, t = null) {
  t = t || db;
  t.collection(collection).doc(doc).update({
    [field]: firebase.firestore.FieldValue.increment(value)
  });
}

export function transaction (cb) {
  return db.runTransaction(cb);
}

export const timestamp = firebase.firestore.FieldValue.serverTimestamp;

/*

Database:
- users [username]:
  - wallet:
    - diamonds
    - coins
- history [username]:
  - [{
    type: buy | sell | send | receive
    amount: XX,
    from: user that received the item,
    to: user that item was sent from,
    item: item sent or received
  }]

*/
