
import { get, set, collection, transaction, doc, increment, add, update, append, timestamp } from './firebase';

export const LOGIN  = 'LOGIN';
export const TIP    = 'TIP';
export const TIPPED = 'TIPPED';

import { diamonds, coins } from '../constants/items';

const defaultUser = {
  wallet: {
    diamonds: 0,
    coins: 0
  }
};

const defaultTips = {}

export const login = username => {
  return async (dispatch) => {
    let userDoc = await get('users', username);

    var history = [];
    var tips = [];

    // add user if it doesn't exist
    if (!userDoc) {
      set('users', username, defaultUser);
      set('history', username, []);
    } else {
      var history = await get('history', username);
      var tipsSent = await get(
        collection('tips')
        .where('from', '==', username)
        .orderBy('timestamp', 'desc')
        .limit(5)
      );
      var tipsReceived = await get(
        collection('tips')
        .where('to', '==', username)
        .orderBy('timestamp', 'desc')
        .limit(5)
      );
      var tips = {...tipsSent, ...tipsReceived};
    }

    var firstSnapshot = true;
    let query = collection('tips')
      .where('to', '==', username)
      .orderBy('timestamp', 'desc')
      .limit(1);
    query.onSnapshot(snap => {
      if (firstSnapshot) {
        firstSnapshot = false;
        return;
      }
      if (snap.size) {
        var data = snap.docs[0].data();
        console.log(data);
        dispatch({
          type: TIPPED,
          from: data.from,
          itemType: data.type,
          amount: data.amount,
          db: {
            id: snap.docs[0].id,
            data
          }
        });
      }
    }, err => {
      console.log(`Encountered error: ${err}`);
    });

    dispatch({
      type: LOGIN,
      username,
      history,
      tips,
      ...userDoc
    });
  }
}

export const getHistory = username => {
  return get('history', username);
}

export const getUsers = () => {
  return get('users');
}


export const tip = (from, to, type, amount) => {
  return async (dispatch) => {
    userTips = doc('tips', from);
    recipientTips = doc('tips', to);
    user = doc('users', from)
    recipient = doc('users', to)

    console.log(from);
    console.log(to);
    console.log(type);
    console.log(amount);

    let res = await transaction(async t => {
      //const docUserTips = await t.get(userTips);

      // all reads done before all writes
      const docUser = await t.get(user);
      const docRecipient = await t.get(recipient);

      let newDiamonds = docUser.data().wallet.diamonds - amount*diamonds(type);
      t.update(user, { "wallet.diamonds" : newDiamonds });

      let newCoins = docRecipient.data().wallet.coins + amount*coins(type);
      t.update(recipient, { "wallet.coins" : newCoins });

      //const docRecipientTips = await t.get(recipientTips);
      /*t.append(userTips, 'sent', {
        type,
        amount
      });*/
      /*t.append(recipientTips, 'received', {
        type,
        amount
      });*/
    })

    let tipReference = await add('tips', {
      from,
      to,
      type,
      amount,
      timestamp: timestamp()
    });

    let tip = await get(tipReference);

    dispatch({
      type: TIP,
      to,
      itemType: type,
      amount,
      db: {
        id: tipReference.id,
        data: tip
      }
    });
  }
}
